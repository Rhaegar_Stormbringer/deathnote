# Death Note

Go ahead. Become the God. Of this New World.

## Description
Unity remake of the "Death Note Type" game made by ([???](https://google.com)). The player takes on the role of Light Yagami, aka Kira, in his quest to purge humanity of the evil that inhabits it and create a new world of which they will be the God.

### Copyright
This game is a fan-made game inspired by the shonen manga "[Death Note](https://fr.wikipedia.org/wiki/Death_Note)" made by [Takeshi Obata](https://fr.wikipedia.org/wiki/Takeshi_Obata) and published in the [Weekly Shōnen Jump](https://fr.wikipedia.org/wiki/Weekly_Sh%C5%8Dnen_Jump) from 2003 to 2006, as well as by its anime adaptation "[Death Note](https://fr.wikipedia.org/wiki/Death_Note)" produced by [Madhouse studio](https://fr.wikipedia.org/wiki/Madhouse_(studio)) in 2006. All assets, references, musics, or elements of these works used in this application belong to their respective authors of origin and are diverted here only to produce an entirely free little game.

## Gameplay
 At the beginning of the game, the player is put in front of Kira's desk with a set of elements placed on it. The player can interact with each of these elements and each has its own function  gameplay-wise:
- [ ] **The Death Note** which is the main interactive object of the game, in which the player will write name of people that should die in order to create their perfect new world.
- [ ] **A pack of Potato Chips** from which the player can take ONE CHIPS, and EAT IT.
- [ ] **A handheld television** which displays some worldwide news, with multiple channels.
- [ ] **An apple** which allows the player to take a break from their quest of Justice by leaving they desk for a walk.
- [ ] **A pencil** that the player can use to change their writing's color.
- [ ] **A computer** which can be interacted with in order to manage the game's options.

### Screens
The game is divided into several "screens" in which the possible actions by the player are different: the "Start" screen, the "Death Note" screen, the "Computer" screen, and the "Outside World" screen.

#### Start Screen
This screen is the one described in the introductory paragraph of the "Gameplay" part. In this screen, the player can interact with all the object of the game, passing from one to another using the `move left`, `move right`, `move up` and `move down` keys and activating them with the `interact` key.

**The handheld television** can be turned on or off using the `interact` key. While on, the displayed channel can be changed using the `move left` or `move right` keys. Using the `move down` key will return the player to the Start screen.

**The pencil** allows the player to change the color with which they write on the Death Note. Upon interaction, a new menu appears that allows the player to enter 3 numbers corresponding to the RGB value of the new color - thus ranging from 0 to 255. This menu is also accompanied by a visual aid in the form of a color wheel, allowing to visualize the new color before validation/ The `move up` and `move down` keys allows to switch from one value to another, or to validate the new color by selecting the "OK" button and pressing the `interact` key. Confirming this new color returns the player to the Death Note screen.

#### Death Note Screen
In this screen, the player can only interact with the **Death Note**, the **pack of Potato Chips**, and the **handheld television**. By default the focus of the player's actions is centered on the **Death Note**, but the player can shift this focus to the **handheld television** using the `move up` key. The `move down` key is used to close the screen and return to the "Start" screen.

**The Death Note** when focused allows the player to use all of the alphanumeric keys on their keyboard to write at will. Its pages can be turned using the `move left` and `move right` keys. The front cover reminds the rules of the Death Note - see the "Gameplay Loop" section.

**The handheld television** when focused will behave like in the Start screen, and can be turned on or off using the `interact` key. While on, the displayed channel can be changed using the `move left` or `move right` keys. Using the `move down` key will return the player's focus on the **Death Note**.

**The pack of Potato Chips** can be interacted using the `additional interact` key and will display a little animation of the player taking a Chips, and eating it with great style.

#### Computer Screen
TODO, basically contains options and stat

#### Outside World Screen
TODO

### Gameplay Loop
While the game does not present the player with any defined objectives, it does fit into the universe created by [Takeshi Obata](https://fr.wikipedia.org/wiki/Takeshi_Obata) which should naturally lead the player to use the Death Note to purge the world of its nastier elements - whatever that term means to the player. The goal is to simulate as much as possible the rules governing the use of the Death Note, and the consequences that such use may have on the game world - and therefore the player's feeling.

#### Deaht Note's rules


#### Player's actions consequences

## Accessibility 
The game can be played entirely with a keyboard. Keys can be rebind in the Start screen. Default bindings are as follow:

<kbd>E</kbd>: `interact`

<kbd>↑</kbd>: `move up`

<kbd>↓</kbd>: `move down`

<kbd>→</kbd>: `move right`

<kbd>←</kbd>: `move left`

<kbd>Shift</kbd>: `additional interact`

## Visuals
TODO, to check: ttygif, Asciinema.

## Installation
TODO, the game should be available as a standalone web application on itch.io.


## Support
Email address: etheric.gamestudio@gmail.com

## Roadmap
TODO

## Contributing
TODO, not open to contributions.

## Authors and acknowledgment
Rhaegar Stormbringer

The Swaglord Dracool

## License
The provided source code, images, sprites, graphical assets and 3D models, are licensed under the [MIT License](http://opensource.org/licenses/MIT).

The MIT License (MIT)

Copyright (c) 2020 [The Swaglord Dracool](https://gitlab.com/TheSwaglordDracool) & [Rhaegar Stormbringer](https://gitlab.com/Rhaegar_Stormbringer)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


