using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
  [SerializeField]
  GameObject normalTextZone;
  [SerializeField]
  GameObject specialTextZone;

  public Queue<Signal> signals = new Queue<Signal>();

  public Signal GetSignal()
  {
    return this.signals.Dequeue();
  }

  public void AddSignal(Signal s)
  {
    this.signals.Enqueue(s);
  }

  bool ProcessInputSignal(Signal s)
  {
    bool processed = false;

    switch(s.function.ToUpper())
    {
      case "KEYDOWN":
        if (s.args.ContainsKey("key"))
        {
          this.normalTextZone.GetComponent<Text>().text += s.args["key"];
        }
        processed = true;
        break;

      case "SPECIALDOWN":
        if (s.args.ContainsKey("key"))
        {
          switch(s.args["key"].ToUpper())
          {
            case "RETURN":
              this.specialTextZone.GetComponent<Text>().text = "Player Entered: "+this.normalTextZone.GetComponent<Text>().text;
              this.normalTextZone.GetComponent<Text>().text ="";
              break;
            case "BACKSPACE":
              this.specialTextZone.GetComponent<Text>().text = "Player Suppressed: "+this.normalTextZone.GetComponent<Text>().text;
              this.normalTextZone.GetComponent<Text>().text ="";
              break;
            default:
              break;

          }
        }
        processed = true;
        break;

      default:
        Debug.Log("[GameManager][ProcessInputSignal] ERROR: Signal's function "+s.function+" unknown");
        processed = true;
        break;
    }

    return processed;
  }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      if (this.signals?.Count != 0)
      {
        bool processed = false;
        Signal s = this.GetSignal();

        switch(s.sender)
        {
          case "InputManager":
            processed = this.ProcessInputSignal(s);
            break;

          default:
            Debug.Log("[GameManager] ERROR: Signal's sender "+s.function+" unknown");
            break;
        }

        if (!processed)
        {
          this.AddSignal(s);
        }
      }
    }
}
