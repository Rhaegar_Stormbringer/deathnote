using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Signal
{
    public String sender = null ;
    public String function = null;
    public Dictionary<String, String> args = new Dictionary<String, String>();

    public Signal(String sender, String function, Dictionary<String, String> args)
    {
      this.sender = sender;
      this.function = function;
      this.args = args;
    }
}
