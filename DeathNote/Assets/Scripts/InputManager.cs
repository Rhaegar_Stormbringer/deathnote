using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    GameObject gameManager;

    Array kcodes = Enum.GetValues(typeof(KeyCode));
    Dictionary<KeyCode, String> normalKeys = new Dictionary<KeyCode, String>
    {
      {KeyCode.A, "A"},
      {KeyCode.B, "B"},
      {KeyCode.C, "C"},
      {KeyCode.D, "D"},
      {KeyCode.E, "E"},
      {KeyCode.F, "F"},
      {KeyCode.G, "G"},
      {KeyCode.H, "H"},
      {KeyCode.I, "I"},
      {KeyCode.J, "J"},
      {KeyCode.K, "K"},
      {KeyCode.L, "L"},
      {KeyCode.M, "M"},
      {KeyCode.N, "N"},
      {KeyCode.O, "O"},
      {KeyCode.P, "P"},
      {KeyCode.Q, "Q"},
      {KeyCode.R, "R"},
      {KeyCode.S, "S"},
      {KeyCode.T, "T"},
      {KeyCode.U, "U"},
      {KeyCode.V, "V"},
      {KeyCode.W, "W"},
      {KeyCode.X, "X"},
      {KeyCode.Y, "Y"},
      {KeyCode.Z, "Z"},
      {KeyCode.Keypad0, "0"},
      {KeyCode.Keypad1, "1"},
      {KeyCode.Keypad2, "2"},
      {KeyCode.Keypad3, "3"},
      {KeyCode.Keypad4, "4"},
      {KeyCode.Keypad5, "5"},
      {KeyCode.Keypad6, "6"},
      {KeyCode.Keypad7, "7"},
      {KeyCode.Keypad8, "8"},
      {KeyCode.Keypad9, "9"},
      {KeyCode.Alpha1, "&"},
      {KeyCode.Alpha2, "é"},
      {KeyCode.Alpha3, "''"},
      {KeyCode.Alpha4, "'"},
      {KeyCode.Alpha5, "("},
      {KeyCode.Alpha6, "-"},
      {KeyCode.Alpha7, "è"},
      {KeyCode.Alpha8, "_"},
      {KeyCode.Alpha9, "ç"},
      {KeyCode.Alpha0, "à"},
      {KeyCode.LeftBracket, ")"},
      {KeyCode.Equals, "="},
      {KeyCode.Space, " "},
      {KeyCode.Semicolon, "$"},
    };
    Dictionary<KeyCode, String> specialKeys = new Dictionary<KeyCode, String>
    {
      {KeyCode.Return, "RETURN"},
      {KeyCode.Backspace, "BACKSPACE"},
    };

    void SendSignal(String receiver, String function, String key)
    {
      Signal s = null;
      switch(receiver)
      {
        case "GameManager":
          s = new Signal("InputManager", function, new Dictionary<String, String>{{"key",key}});
          this.gameManager.GetComponent<GameManager>().AddSignal(s);
          break;

        // default to GameManager
        default:
          s = new Signal("InputManager", function, new Dictionary<String, String>{{"key",key}});
          this.gameManager.GetComponent<GameManager>().AddSignal(s);
          break;

      }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        foreach (KeyCode kcode in kcodes)
        {
            if (Input.GetKeyDown(kcode))
            {
                // 1 - Checks if special keys
                if (specialKeys.ContainsKey(kcode))
                {
                    SendSignal("GameManager", "SPECIALDOWN", specialKeys[kcode]);
                }
                // 2 - Else checks if normal keys
                else if (normalKeys.ContainsKey(kcode))
                {
                    SendSignal("GameManager", "KEYDOWN", normalKeys[kcode]);

                }
                // 3- Else raises warning
                else
                {
                    Debug.Log("NOK "+kcode.ToString());
                }
            }



        }
    }

}
