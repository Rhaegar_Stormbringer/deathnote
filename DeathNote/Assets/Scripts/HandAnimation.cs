using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandAnimation : MonoBehaviour
{
    private Animator anim;
    

    private void Start()
    {
        anim = GetComponent<Animator>();
        
    }


    private void Update()
    {
        RectTransform myRect = GetComponent<RectTransform>();

        if (Input.anyKeyDown)
        {
            anim.SetBool("isWriting", true);
            myRect.anchoredPosition3D = new Vector3(-200, 7, 0);
        }
        else
        {
            anim.SetBool("isWriting", false);
        }
    }
}
